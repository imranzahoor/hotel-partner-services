<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');

require_once("./SplClassLoader.php");
$loader = new SplClassLoader();
$loader->register();

use Hotel\Service\PartnerService;
use Hotel\Service\PartnerNameOrderedHotelService;
use Hotel\Service\PriceOrderedHotelService;

$dataSource = "../data/";

$oPartnerService = new PartnerService($dataSource);
$orderdPartnerService = new PartnerNameOrderedHotelService($oPartnerService);

$orderdPriceOrderService = new PriceOrderedHotelService($oPartnerService);

$result = $orderdPartnerService->getHotelsForCity('Düsseldorf');

$resultPrice = $orderdPriceOrderService->getHotelsForCity('Düsseldorf');
echo "<pre>Price Sorted List-------------<br>";
print_r($resultPrice);

echo "<pre>Partner Name Sorted List-------------<br>";
print_r($result);