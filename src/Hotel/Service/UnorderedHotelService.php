<?php

namespace Hotel\Service;

/**
 * This class is an implementation of an unordered hotel service.
 */
class UnorderedHotelService implements HotelServiceInterface {

    /**
     * @var PartnerServiceInterface
     */
    private $oPartnerService;
    
    /**
     * @var unorderedPartners
     */
    
    private $partners;

    /**
     * Maps from city name to the id for the partner service.
     *  
     * @var array
     */
    private $aCityToIdMapping = array(
        "Düsseldorf" => 15475
    );

    /**
     * @param PartnerServiceInterface $oPartnerService
     */
    public function __construct(PartnerServiceInterface $oPartnerService) {
        $this->oPartnerService = $oPartnerService;
    }

    /**
     * @inherited
     */
    public function getHotelsForCity($sCityName) {
        if (!isset($this->aCityToIdMapping[$sCityName])) {
            throw new \InvalidArgumentException(sprintf('Given city name [%s] is not mapped.', $sCityName));
        }

        $iCityId = $this->aCityToIdMapping[$sCityName];
        $this->setPartners($this->oPartnerService->getResultForCityId($iCityId));
    }
    
    public function setPartners($partners) {
        $this->partners = $partners;
    }
    
    public function getPartners() {
        return $this->partners;
    }

}
