<?php

namespace Hotel\Service;

/**
 * Sorts partner services based on their names
 *
 */
class PartnerNameSortService extends SortService {

    public function sortData($hotels) {
        foreach ($hotels as $hotelKey => $hotel) {
            if (!empty($hotel->aPartners)) {
                if(\uasort($hotel->aPartners, array($this, $this->mode))) {
                    $hotels[$hotelKey]->aPartners = $hotel->aPartners;
                }
            }
        }
        return $hotels;
    }

}
