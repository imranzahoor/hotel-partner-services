<?php

namespace Hotel\Service;

/**
 * Generic validation service. Which provides custom validations methods and those methods are accessible through validate function
 */
class ValidationService {

    /**
     * Validates the given object on given key against given validation method
     * @param Object $objectOrArray Object or Array or even string
     * @param string $key
     * @param string $validation validation function, availabe function is isValidUrl
     */
    public function validate($objectOrArray, $key, $validation) {
        if (!method_exists(get_class(), $validation)) {
            throw new \InvalidArgumentException(sprintf('validation method [%s] is not supported unknown.', $validation));
        }

        if (is_object($objectOrArray)) {
            return $this->$validation($objectOrArray->$key);
        } else if (is_array($objectOrArray)) {
            return $this->$validation($objectOrArray[$key]);
        } else if (is_string($objectOrArray)) {
            return $this->$validation($objectOrArray);
        } else {
            throw new \InvalidArgumentException(sprintf('objectOrArray [%s] is not supported type.', $objectOrArray));
        }
    }

    /**
     * Checks weather a Url is valid or not, Url without http:// or https:// are taken as invalid URLs
     * @param type $url
     * @return boolean True on valid and false if invalid
     */
    public function isValidUrl($url) {
        if (filter_var($url, FILTER_VALIDATE_URL) !== false) {
            return true;
        }
        return false;
    }

}
