<?php

namespace Hotel\Service;

use Hotel\Entity\Hotel;
use Hotel\Entity\Partner;
use Hotel\Entity\Price;

/**
 * Description of PartnerService
 *
 */
class PartnerService implements PartnerServiceInterface {

    protected $dataSourcePath;
    protected $validationService;

    /**
     * @param type $dataSourcePath path where data sources for cities exist
     */
    public function __construct($dataSourcePath) {
        //check if there is no trailing slash '/', attach it.
        if ($dataSourcePath[strlen($dataSourcePath) - 1] != '/') {
            $dataSourcePath .= '/';
        }
        $this->dataSourcePath = $dataSourcePath;
        $this->validationService = new ValidationService();
    }

    /**
     * Load data from json data source as PHP array
     * @return array
     * @throws \InvalidArgumentException
     */
    public function loadJSONAsArray($iCityId) {
        $json = \json_decode(\utf8_encode(\file_get_contents($this->dataSourcePath . $iCityId . ".json")), true);
        if (empty($json)) {
            throw new \InvalidArgumentException(sprintf("Sorry we cannot locate the data source for the selected city."));
        }
        return $json;
    }

    /**
     * @implemented
     */
    public function getResultForCityId($iCityId) {
        $json = $this->loadJSONAsArray($iCityId);
        $hotels = array();
        foreach ($json['hotels'] as $hotelKey => $hotel) {
            $oHotel = new Hotel();
            $oHotel->sName = $hotel['name'];
            $oHotel->sAdr = $hotel['adr'];
            if (!empty($hotel['partners'])) {
                $this->validateAndLoadPartners($hotel, $oHotel);
            }
            $hotels[$hotelKey] = $oHotel;
        }
        return $hotels;
    }

    /**
     * Attaches partner and price data part for valid partners and ignores invalid
     * @param array $hotel
     * @param Hotel $oHotel Hotel class instance
     */
    private function validateAndLoadPartners($hotel, Hotel &$oHotel) {
        foreach ($hotel['partners'] as $partnerKey => $partner) {
            //ensure that Url is valid through validator service, if not valid move to next partner
            //checking it before assignment, to avoid unnecessory processing
            if (!$this->validationService->validate($partner['url'], '', 'isValidUrl')) {
                continue;
            }
            $oPartner = new Partner();
            $oPartner->sName = $partner['name'];
            $oPartner->sHomepage = $partner['url'];
            if (!empty($partner['prices'])) {
                foreach ($partner['prices'] as $priceKey => $price) {
                    $oPrice = new Price();
                    $oPrice->sDescription = $price['description'];
                    $oPrice->fAmount = $price['amount'];
                    $oPrice->oFromDate = $price['from'];
                    $oPrice->oToDate = $price['to'];
                    $oPartner->aPrices[$priceKey] = $oPrice;
                }
            }
            $oHotel->aPartners[$partnerKey] = $oPartner;
        }
    }

}
