<?php

namespace Hotel\Service;

use Hotel\Entity\Hotel;

/**
 * Sorts the array based on the key and order (ASC = Ascending, DESC=Descending)
 */
abstract class SortService {

    protected $mode;
    protected $key;

    /**
     * @param string $key key to be sorted
     * @param int $mode 'asc' for Ascending order, 'desc' for descending order
     */
    public function __construct($key, $mode) {
        $this->key = $key;
        $this->mode = strtolower($mode);
        if (!in_array($this->mode, array('asc', 'desc'))) {
            throw new \InvalidArgumentException(sprintf('Given mode [%s] is unknown.', $mode));
        }
    }

    /**
     * Sorts the data on Hotel class instance
     * @param array &$array
     * @return array
     */
    public abstract function sortData($array);

    public function desc($a, $b) {
        $a = (array) $a;
        $b = (array) $b;
        if ($a[$this->key] == $b[$this->key]) {
            return 0;
        }
        if ($a[$this->key] > $b[$this->key]) {
            return -1;
        }
        return 1;
    }

    public function asc($a, $b) {
        $a = (array) $a;
        $b = (array) $b;
        if ($a[$this->key] == $b[$this->key]) {
            return 0;
        }
        if ($a[$this->key] < $b[$this->key]) {
            return -1;
        }
        return 1;
    }

}
