<?php
namespace Hotel\Service;

/**
 * Sorts price list based on the amount
 *
 */
class PriceSortService extends SortService {
    public function sortData($hotels) {
        foreach ($hotels as $hotelKey => $hotel) {
            if (!empty($hotel->aPartners)) {
                foreach ($hotel->aPartners as $partnerKey => $partner) {
                    if(\uasort($partner->aPrices, array($this, $this->mode))) {
                        $partner->aPrices = $partner->aPrices;
                    }
                }
                $hotels[$hotelKey]->aPartners = $hotel->aPartners;
            }
        }
        return $hotels;
    }
}
