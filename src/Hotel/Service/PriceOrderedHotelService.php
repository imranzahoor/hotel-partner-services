<?php

namespace Hotel\Service;

/**
 * This class is for ordered hotel service based on their partner prices.
 *
 */
class PriceOrderedHotelService extends UnorderedHotelService {

    /**
     * @overriden
     */
    public function getHotelsForCity($sCityName) {
        parent::getHotelsForCity($sCityName);//this will set the partners as well
        $oPriceOrder = new PriceSortService('fAmount', 'asc'); //you can pass 'desc' to sort it in descending order
        return $oPriceOrder->sortData($this->getPartners());
    }

}
