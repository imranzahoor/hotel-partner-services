<?php

namespace Hotel\Service;

/**
 * This class is for ordered hotel service based on their partner names.
 *
 */
class PartnerNameOrderedHotelService extends UnorderedHotelService {

    /**
     * @overriden
     */
    public function getHotelsForCity($sCityName) {
        parent::getHotelsForCity($sCityName);//this will set the partners as well
        $oPartnerNameOrder = new PartnerNameSortService('sName', 'asc'); //you can pass 'desc' to sort it in descending order
        return $oPartnerNameOrder->sortData($this->getPartners());
    }

}
