<?php
require __DIR__  . '/../src/SplClassLoader.php';

$oClassLoader = new \SplClassLoader('Hotel', __DIR__ . '/../src');
$oClassLoader->register();
