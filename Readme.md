# Hotel Partner Services
This is php-casestudy or test project, which is developed on idea of taking JSON based datasource. 
The partners of some site are forced to provide data in JSON form. The datasource will be further used by this application and 
data will be actually mapped to classes. And then data will be further used to achieve different kind of sorting e.g. price based sorting, name based sorting.

## What it does on technical grounds

### Implements PartnerService
Makes use of JSON file in data directory as datasource. Partner Service interface is introduced and implemented. 
The values are read from JSON and are passed to corresponding classes within Entity namespace.

The entities encapsulate each other:

`Hotel --> <hasMany> --> Partner --> <hasMany> --> Price`

The JSON file has similar kind of structure.

### A basic validator 

Implemented a basic validation service inside `Hotel\Service\ValidationService`. The class is used while reading data from datasource. 
As we ensure through this class that we only read those datasources which are with valid URLs.

### Offers different implementations for HotelServiceInterface

HotelServiceInterface is implemented by `UnorderedHotelService`, which loads list of Parter Services without sorting from datasource.
`PriceOrderedHotelService` and `PartnerNameOrderedHotelService` extend the `UnorderedHotelService` to sort the result, 
while loading it using existing code of `UnorderedHotelService` :)

### Uses PHPUnit

One can run couple of unit tests written in PHPUnit.

### Testing through browser

One can run it through browser with following URL:
http://localhost/hotel-partner-services/src/TestService.php

## Preconditions

### Technical
You need at least:

* PHP 5.3 or higher

### Purpose
To demonstrate skills related to:
* PHP's OOP implementation, including interfaces.
* Namespaces
* Reading resources from a local file system location
* Coping with JSON as data format

##Browser based output
---
```
Price Sorted List-------------
Array
(
    [1] => Hotel\Entity\Hotel Object
        (
            [sName] => Hilton DÃƒÂ¼sseldorf
            [sAdr] => Georg-Glock-StraÃƒÂŸe 20, 40474 DÃƒÂ¼sseldorf
            [aPartners] => Array
                (
                    [101] => Hotel\Entity\Partner Object
                        (
                            [sName] => Dtestpartner.com
                            [sHomepage] => http://www.testpartner.com
                            [aPrices] => Array
                                (
                                    [1001] => Hotel\Entity\Price Object
                                        (
                                            [sDescription] => Single Room
                                            [fAmount] => 125
                                            [oFromDate] => 2012-10-12
                                            [oToDate] => 2012-10-13
                                        )

                                    [1002] => Hotel\Entity\Price Object
                                        (
                                            [sDescription] => Double Room
                                            [fAmount] => 139
                                            [oFromDate] => 2012-10-12
                                            [oToDate] => 2012-10-13
                                        )

                                )

                        )

                    [102] => Hotel\Entity\Partner Object
                        (
                            [sName] => Atestpartner.com
                            [sHomepage] => http://www.testpartner.com
                            [aPrices] => Array
                                (
                                    [1001] => Hotel\Entity\Price Object
                                        (
                                            [sDescription] => Single Room
                                            [fAmount] => 125
                                            [oFromDate] => 2012-10-12
                                            [oToDate] => 2012-10-13
                                        )

                                    [1002] => Hotel\Entity\Price Object
                                        (
                                            [sDescription] => Double Room
                                            [fAmount] => 139
                                            [oFromDate] => 2012-10-12
                                            [oToDate] => 2012-10-13
                                        )

                                )

                        )

                )

        )

    [2] => Hotel\Entity\Hotel Object
        (
            [sName] => Mercure DÃƒÂ¼sseldorfCity Nord
            [sAdr] => NÃƒÂ¶rdlicher Zubringer 7, 40470 DÃƒÂ¼sseldorf
            [aPartners] => Array
                (
                    [201] => Hotel\Entity\Partner Object
                        (
                            [sName] => Atestpartner.com
                            [sHomepage] => http://www.testpartner.com
                            [aPrices] => Array
                                (
                                    [2001] => Hotel\Entity\Price Object
                                        (
                                            [sDescription] => Single Room
                                            [fAmount] => 52
                                            [oFromDate] => 2012-10-12
                                            [oToDate] => 2012-10-13
                                        )

                                    [2002] => Hotel\Entity\Price Object
                                        (
                                            [sDescription] => Double Room
                                            [fAmount] => 53
                                            [oFromDate] => 2012-10-12
                                            [oToDate] => 2012-10-13
                                        )

                                )

                        )

                )

        )

)

Partner Name Sorted List-------------
Array
(
    [1] => Hotel\Entity\Hotel Object
        (
            [sName] => Hilton DÃƒÂ¼sseldorf
            [sAdr] => Georg-Glock-StraÃƒÂŸe 20, 40474 DÃƒÂ¼sseldorf
            [aPartners] => Array
                (
                    [102] => Hotel\Entity\Partner Object
                        (
                            [sName] => Atestpartner.com
                            [sHomepage] => http://www.testpartner.com
                            [aPrices] => Array
                                (
                                    [1001] => Hotel\Entity\Price Object
                                        (
                                            [sDescription] => Single Room
                                            [fAmount] => 125
                                            [oFromDate] => 2012-10-12
                                            [oToDate] => 2012-10-13
                                        )

                                    [1002] => Hotel\Entity\Price Object
                                        (
                                            [sDescription] => Double Room
                                            [fAmount] => 139
                                            [oFromDate] => 2012-10-12
                                            [oToDate] => 2012-10-13
                                        )

                                )

                        )

                    [101] => Hotel\Entity\Partner Object
                        (
                            [sName] => Dtestpartner.com
                            [sHomepage] => http://www.testpartner.com
                            [aPrices] => Array
                                (
                                    [1002] => Hotel\Entity\Price Object
                                        (
                                            [sDescription] => Double Room
                                            [fAmount] => 139
                                            [oFromDate] => 2012-10-12
                                            [oToDate] => 2012-10-13
                                        )

                                    [1001] => Hotel\Entity\Price Object
                                        (
                                            [sDescription] => Single Room
                                            [fAmount] => 125
                                            [oFromDate] => 2012-10-12
                                            [oToDate] => 2012-10-13
                                        )

                                )

                        )

                )

        )

    [2] => Hotel\Entity\Hotel Object
        (
            [sName] => Mercure DÃƒÂ¼sseldorfCity Nord
            [sAdr] => NÃƒÂ¶rdlicher Zubringer 7, 40470 DÃƒÂ¼sseldorf
            [aPartners] => Array
                (
                    [201] => Hotel\Entity\Partner Object
                        (
                            [sName] => Atestpartner.com
                            [sHomepage] => http://www.testpartner.com
                            [aPrices] => Array
                                (
                                    [2001] => Hotel\Entity\Price Object
                                        (
                                            [sDescription] => Single Room
                                            [fAmount] => 52
                                            [oFromDate] => 2012-10-12
                                            [oToDate] => 2012-10-13
                                        )

                                    [2002] => Hotel\Entity\Price Object
                                        (
                                            [sDescription] => Double Room
                                            [fAmount] => 53
                                            [oFromDate] => 2012-10-12
                                            [oToDate] => 2012-10-13
                                        )

                                )

                        )

                )

        )

)

```